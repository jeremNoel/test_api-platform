<?php

namespace App\DataFixtures\Faker\Provider;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ApplicationProvider
{
    public static function getRandomGender()
    {
        $gender = [
            'Homme',
            'Femme',
            'Non binaire',
            'Non renseigné',
        ];
        return $gender[array_rand($gender)];
    }

    public static function getRandomNumber()
    {
        return rand(18, 45);
    }

    public static function getRandomContract()
    {
        $contract = ["CDD", "CDI", "Stage", "Alternance", "Interim"];
        return $contract[array_rand($contract)];
    }

    public static function getRandomFloat()
    {
        return rand(100000, 200000) / rand(2, 4);
    }

    public static function getRandomToken()
    {
        return 'application_' . uniqid();
    }
}

<?php

namespace App\DataFixtures\Faker\Provider;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserProvider
{
    /** @var string UserPasswordEncoderInterface  */
    private static $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        self::$passwordEncoder = $passwordEncoder;
    }

    public static function encodePassword(string $pwd)
    {
        $user = new User();
        return self::$passwordEncoder->encodePassword($user,$pwd);
    }

    public static function getRandRoles()
    {
        $roles = [
            'ROLE_ANONYMOUS',
            'ROLE_ADMIN',
            'ROLE_USER',
        ];

        return [$roles[array_rand($roles)]];
    }

    public static function tokenUser()
    {
        return 'token_' . uniqid();
    }

    public static function getRandIsActive()
    {
        $isActive = [true, false];
        return $isActive[array_rand($isActive)];
    }
}

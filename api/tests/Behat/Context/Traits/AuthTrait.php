<?php

namespace App\Tests\Behat\Context\Traits;

use GuzzleHttp\Psr7\Request;

trait AuthTrait
{
    /**
     * The token to use with HTTP authentication
     *
     * @var string
     */
    protected $token;

    /**
     * @Given /^I authenticate with user "([^"]*)" and password "([^"]*)"$/
     */
    public function iAuthenticateWithEmailAndPassword($email, $password)
    {
        try {
            $this->lastResponse = $this->client->request(
                "POST",
                "/authentication_token",
                [
                    'headers' => [ 'Content-Type' => 'application/json' ],
                    'body'    => json_encode([
                        'email' => $email,
                        'password' => $password
                    ]),
                ]
            );
        } catch (\Exception $e) {
            $response = $e->getMessage();
            if ($response === null) {
                throw $e;
            }
            $this->lastResponse = $e->getMessage();
            throw new \Exception('Bad response.');
        }

        $response = json_decode($this->lastResponse->getContent(false), false);
        foreach($response as $key => $value)
            if($key == "token")
                $this->token = $value;
    }
}

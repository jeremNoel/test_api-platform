<?php

namespace App\Tests\Behat\Context\Traits;

use App\Tests\Behat\Manager\FixtureManager;
use App\Tests\Behat\Manager\ReferenceManager;
use Behat\Gherkin\Node\TableNode;

trait FixturesTrait
{
    /**
     * @var FixtureManager
     */
    private FixtureManager $fixtureManager;
    private ReferenceManager $referenceManager;

    private $fixtureLoaded;

    /**
     * @Given /^the fixtures file "([^"]*)" is loaded$/
     */
    public function theFixturesFileIsLoaded(string $file)
    {
        $this->fixtureManager->load(['./fixtures/' . $file . '.yml']);
    }

    /**
     * @Given the following fixtures files are loaded:
     */
    public function theFixturesFilesAreLoaded(TableNode $table)
    {
        $files = array_map(fn($row) => './fixtures/' . $row[0] . '.yml', $table->getRows());

        try {
            $this->fixtureManager->load($files);
        } catch (\Exception $e) {
            $response = $e->getMessage();

            if ($response === null) {
                throw $e;
            }
            //dump($e);
            throw new \Exception(
                'Problem with file, uncomment dump in FixturesTrait.php'
            );
        }
    }

    /**
     * @Given random fixture
     */
    public function getRandomFixture()
    {
        $this->fixtureLoaded = $this->fixtureManager->test[array_rand($this->fixtureManager->test)];
    }
}

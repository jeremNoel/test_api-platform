Feature: _Application_
  Background:
    Given the following fixtures files are loaded:
      | users     |
      | offer     |
      | media     |
      | status     |
      | application     |

# All tests with application
	Scenario: Get all applications
		Given I request "GET /applications"
		Then the response status code should be 401


	Scenario: Creates a Application ressource.
		Given I have the payload
		"""
		{
			"name": "eleve",
			"firstName": "eleve",
			"gender": "Homme",
			"email": "ajdpajpjadpj@gmail.com",
			"age": "20",
			"address": "9668 Herminio Rue",
			"offer": {offer_1.id}
		}
		"""
		Given I request "POST /applications"
		Then the response status code should be 401

	Scenario: Creates a Application ressource with admin auth.
		Given I authenticate with user "test@email.com" and password "test"
		Given I have the payload
		"""
		{
			"name": "eleve",
			"firstName": "eleve",
			"gender": "Homme",
			"email": "ajdpajpjadpj@gmail.com",
			"age": "20",
			"address": "9668 Herminio Rue",
			"offer": "{offer_1.id}"
		}
		"""
		Given I request "POST /applications"
		Then the response status code should be 403

	Scenario: Creates a Application ressource with applicant role auth.
		Given I authenticate with user "test_applicant@email.com" and password "test"
		Given I have the payload
		"""
		{
			"name": "eleve",
			"firstName": "eleve",
			"gender": "Homme",
			"email": "ajdpajpjadpj@gmail.com",
			"age": "20",
			"address": "9668 Herminio Rue",
			"offer": "{offer_1.id}"
		}
		"""
		Given I request "POST /applications"
		Then the response status code should be 201

	Scenario: Get one applications
		Given I request "GET /applications/{application_1.id}"
		Then the response status code should be 401

	Scenario: Delete an identified applications
		Given I request "DELETE /applications/{application_1.id}"
		Then the response status code should be 401

	Scenario: Get all applications with auth
		Given I authenticate with user "test@email.com" and password "test"
		Given I request "GET /applications"
		Then the response status code should be 200

	Scenario: GET applications id
		Given I request "GET /applications/{application_1.id}"
		Then the response status code should be 401

	Scenario: PATCH applications id
		Given I request "PATCH /applications/{application_1.id}"
		Then the response status code should be 401

	Scenario: DELETE applications id
		Given I request "DELETE /applications/{application_1.id}"
		Then the response status code should be 401

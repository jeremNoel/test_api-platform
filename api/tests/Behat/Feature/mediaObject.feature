Feature: _media_
  Background:
    Given the following fixtures files are loaded:
      | users     |
      | offer     |
      | media     |
      | status     |
      | application     |


    Scenario: Get list of media object without auth
        Given I request "GET /media_objects"
        And the response status code should be 401

    Scenario: Get list of media object with auth
        Given I authenticate with user "test@email.com" and password "test"
        Given I request "GET /media_objects"
        And the response status code should be 200

    Scenario: Get object with ID
        Given I authenticate with user "test@email.com" and password "test"
        Given I request "GET /media_objects/1"
        And the response status code should be 200

    Scenario: Get object with ID without auth
        Given I request "GET /media_objects/1"
        And the response status code should be 401

    Scenario: Get object with fake ID without auth
        Given I request "GET /media_objects/5476890765"
        And the response status code should be 401

    #this should render 404 and not 200 if they don't find the media
    Scenario: Get object with fake ID
        Given I authenticate with user "test@email.com" and password "test"
        Given I request "GET /media_objects/5476890765"
        And the response status code should be 404

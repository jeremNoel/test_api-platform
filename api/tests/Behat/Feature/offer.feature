Feature: _Offer_
  Background:
    Given the following fixtures files are loaded:
      | users     |
      | offer     |
      | media     |
      | status     |
      | application     |

## All tests with unauthorized user

# All tests with application
	Scenario: Get all offers
		Given I request "GET /offers"
		Then the response status code should be 401

    Scenario: Get all offers with auth
		Given I authenticate with user "test@email.com" and password "test"
		Given I request "GET /offers"
		Then the response status code should be 200

    Scenario: Get all offer owned with an unauthorized user
        Given I request "GET offers/owned"
        Then the response status code should be 404

    Scenario: Get all offer owned with an auth
		Given I authenticate with user "test@email.com" and password "test"
        Given I request "GET offers/owned"
        Then the response status code should be 200

    Scenario: Creates a offer ressource with an unauthorized user. 
		Given I have the payload
		"""
		{
            "name": "president de la république",
            "descriptionCompany": "Un gouvernement pour les gouverner tous",
            "description": "Choisir de baisser les impots ou de les augmentés",
            "typeContract": "CDD",
            "workplace": "Élysée",
            "applications": [
                {application_1.id}
            ],
            "recruiter": "Mehdi le BG"
        }
		"""
		Given I request "POST /offers"
		Then the response status code should be 401

     Scenario: Creates a offer ressource with auth user. 
		Given I authenticate with user "test@email.com" and password "test"
		Given I have the payload
		"""
		{
            "name": "president de la république",
            "descriptionCompany": "Un gouvernement pour les gouverner tous",
            "description": "Choisir de baisser les impots ou de les augmentés",
            "typeContract": "CDD",
            "workplace": "Élysée",
            "applications": [
                {application_1.id}
            ],
            "recruiter": "Mehdi le BG"
        }
		"""
		Given I request "POST /offers"
		Then the response status code should be 403

    Scenario: PATCH offer id with an unauthorized user.
		Given I request "PATCH /offers/{offer_1.id}"
		Then the response status code should be 401

    Scenario: PATCH offer id with auth users.
		Given I authenticate with user "test@email.com" and password "test"
		Given I request "PATCH /offers/{offer_1.id}"
		Then the response status code should be 404

    Scenario: PUT offer id with an unauthorized user.
		Given I request "PUT /offers/{offer_1.id}"
		Then the response status code should be 401

    Scenario: PUT offer id with an auth user.
		Given I authenticate with user "test@email.com" and password "test"
		Given I request "PUT /offers/{offer_1.id}"
		Then the response status code should be 404

    Scenario: DELETE offer id with an unauthorized user.
		Given I request "DELETE /offers/{offer_1.id}"
		Then the response status code should be 401
    
    Scenario: DELETE offer id with auth user.
		Given I authenticate with user "test@email.com" and password "test"
		Given I request "DELETE /offers/{offer_1.id}"
		Then the response status code should be 404

Feature: _Status_
  Background:
    Given the following fixtures files are loaded:
      | users     |
      | offer     |
      | media     |
      | status     |
      | application     |

## All tests with unauthorized user

# All tests with status
	Scenario: Get all status
		Given I request "GET /statuses"
		Then the response status code should be 401

    Scenario: Get all status with auth
		Given I authenticate with user "test@email.com" and password "test"
		Given I request "GET /statuses"
		Then the response status code should be 200
        
    Scenario: Get one status
		Given I request "GET /statuses/{status_1.id}"
		Then the response status code should be 401

    Scenario: Get one status with auth
		Given I request "GET /statuses/{status_1.id}"
		Then the response status code should be 401

    Scenario: Creates a status ressource with an unauthorized user. 
		Given I have the payload
		"""
		{
            "name": "status_20",
            "applications": [
                {application_1.id}
            ]
        }
		"""
		Given I request "POST /statuses"
		Then the response status code should be 401

     Scenario: Creates a status ressource with auth user. 
		Given I authenticate with user "test@email.com" and password "test"
		Given I have the payload
		"""
		{
            "name": "status_20",
            "applications": [
                {application_1.id}
            ]
        }
		"""
		Given I request "POST /statuses"
		Then the response status code should be 201

    Scenario: PATCH status id with an unauthorized user.
		Given I request "PATCH /statuses/{status_1.id}"
		Then the response status code should be 401

    #Scenario: PATCH status id with auth users.
	#	Given I authenticate with user "test@email.com" and password "test"
	#	Given I request "PATCH /statuses/{status_1.id}"
	#	Then the response status code should be 404

    Scenario: PUT status id with an unauthorized user.
		Given I request "PUT /statuses/{status_1.id}"
		Then the response status code should be 401

    Scenario: PUT status id with an auth user.
		Given I authenticate with user "test@email.com" and password "test"
		Given I request "PUT /statuses/{status_1.id}"
		Then the response status code should be 400

    Scenario: DELETE status id with an unauthorized user.
		Given I request "DELETE /statuses/{status_1.id}"
		Then the response status code should be 401
    
    #Scenario: DELETE status id with auth user.
	#	Given I authenticate with user "test@email.com" and password "test"
	#	Given I request "DELETE /statuses/{status_1.id}"
	#	Then the response status code should be 404

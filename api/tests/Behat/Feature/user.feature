Feature: _User_
  Background:
    Given the following fixtures files are loaded:
      | users     |
      | offer     |
      | media     |
      | status     |
      | application     |

# All tests with unauthorized user
  Scenario: Get all users with an unauthorized user
    Given I request "GET /users"
    And the response status code should be 401

  Scenario: Subscribe with unauthentified user
    Given I have the payload
    """
    {
        "email": "azerbaidjan@email.com",
        "password": "test",
        "isActive": true
    }
    """
    Given I request "POST /users"
    And the response status code should be 201

#  Scenario: Get one user with an unauthorized user
#    And the response status code should be 401

 # Scenario: Updates the User resource
  #  Given I request "PUT users"
  #  And the response status code should be 401

# All tests with authorized user
  Scenario: Get all users with an authorized user with role admin
    Given I authenticate with user "test@email.com" and password "test"
    Given I request "GET /users"
    And the response status code should be 200

  Scenario: Get connected user authorized with role admin
    Given I authenticate with user "test@email.com" and password "test"
    Given I request "GET /users/{user_role_user.id}"
    And the response status code should be 200

  Scenario: Get one user with an other user authorized with role admin
    Given I authenticate with user "test@email.com" and password "test"
    Given I request "GET /users/{user_1.id}"
    And the response status code should be 403

  Scenario: Patch an identified user with same authentified user
    Given I authenticate with user "test@email.com" and password "test"
    Given I have the payload
    """
    {
      "email": "emlemlmlregeg@sdfs.com",
      "roles": [
        "ROLE_USER"
      ],
      "password": "test",
      "isActive": true,
      "token": "token"
    }
    """
    Given I request "PATCH /users/{user_role_user.id}"
    And the response status code should be 200

  Scenario: Delete an identified user with same authentified user
    Given I authenticate with user "test@email.com" and password "test"
    Given I request "DELETE /users/{user_role_user.id}"
    And the response status code should be 200

<?php

namespace App\Tests\Behat\Manager;

class ReferenceManager
{
    /** @var string $path */
    private $path = null;
    /** @var mixed $ref */
    private $ref = null;

    /**
     * @param $ressource
     * @param $fixtures
     * @return string|null
     */
    public function getPath($ressource, $fixtures)
    {
        $this->runPath($ressource, $fixtures);
        return $this->path;
    }

    public function replaceRef($ressource, $fixtures)
    {
        $this->runReplaceRef($ressource, $fixtures);
        return $this->ref;
    }

    public function runReplaceRef($ressource, $fixtures)
    {
        $reference = "";
        preg_match('/{(.*?)}/', strval($ressource), $matches);
        if(isset($matches) && isset($matches[1])) {
            $identifier = preg_split('/\./', strval($matches[1]));
            $request = "get" . ucfirst($identifier[1]);
            $reference = $fixtures[$identifier[0]]->$request();
            $this->ref = str_replace(
                $matches[0],
                strval($reference),
                $ressource
            );
        }
    }

    /**
     * @param string $ressource
     * @param array $fixtures
     */
    private function runPath($ressource, $fixtures)
    {
        $matches = [];
        $reference = "";
        $baseRequest = preg_split('/\//', $ressource);
        if(isset($baseRequest[1])) {
            $baseRequest = $baseRequest[1];

            preg_match('/{(.*?)}/', $ressource, $matches);
            if(isset($matches) && isset($matches[1])) {
                $identifier = preg_split('/\./', strval($matches[1]));
                $request = "get" . ucfirst($identifier[1]);
                $reference = '/' . $fixtures[$identifier[0]]->$request();
            }
            $this->path = '/'. $baseRequest . $reference;
        }
    }
}
